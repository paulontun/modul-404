namespace Schachbrett
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ActionPanel = new System.Windows.Forms.Panel();
            this.ApplyRows = new System.Windows.Forms.Button();
            this.SelectRowNumber = new System.Windows.Forms.NumericUpDown();
            this.ChessArea = new System.Windows.Forms.Panel();
            this.ActionPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize) (this.SelectRowNumber)).BeginInit();
            this.SuspendLayout();
            // 
            // ActionPanel
            // 
            this.ActionPanel.BackColor = System.Drawing.Color.Black;
            this.ActionPanel.Controls.Add(this.ApplyRows);
            this.ActionPanel.Controls.Add(this.SelectRowNumber);
            this.ActionPanel.Location = new System.Drawing.Point(12, 12);
            this.ActionPanel.Name = "ActionPanel";
            this.ActionPanel.Size = new System.Drawing.Size(416, 77);
            this.ActionPanel.TabIndex = 0;
            // 
            // ApplyRows
            // 
            this.ApplyRows.Location = new System.Drawing.Point(129, 15);
            this.ApplyRows.Name = "ApplyRows";
            this.ApplyRows.Size = new System.Drawing.Size(76, 43);
            this.ApplyRows.TabIndex = 1;
            this.ApplyRows.Text = "Start";
            this.ApplyRows.UseVisualStyleBackColor = true;
            this.ApplyRows.Click += new System.EventHandler(this.ApplyRows_Click);
            // 
            // SelectRowNumber
            // 
            this.SelectRowNumber.Increment = new decimal(new int[] {2, 0, 0, 0});
            this.SelectRowNumber.Location = new System.Drawing.Point(30, 27);
            this.SelectRowNumber.Maximum = new decimal(new int[] {30, 0, 0, 0});
            this.SelectRowNumber.Name = "SelectRowNumber";
            this.SelectRowNumber.Size = new System.Drawing.Size(70, 23);
            this.SelectRowNumber.TabIndex = 0;
            // 
            // ChessArea
            // 
            this.ChessArea.BackColor = System.Drawing.Color.White;
            this.ChessArea.Location = new System.Drawing.Point(12, 95);
            this.ChessArea.Name = "ChessArea";
            this.ChessArea.Size = new System.Drawing.Size(416, 412);
            this.ChessArea.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(436, 519);
            this.Controls.Add(this.ChessArea);
            this.Controls.Add(this.ActionPanel);
            this.Name = "Form1";
            this.Text = "Schachbrett";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ActionPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize) (this.SelectRowNumber)).EndInit();
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Panel ActionPanel;
        private System.Windows.Forms.Button ApplyRows;
        private System.Windows.Forms.Panel ChessArea;
        private System.Windows.Forms.NumericUpDown SelectRowNumber;
    }
}