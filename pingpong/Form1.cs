using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace pingpong
{
    public partial class Form1 : Form
    {
        //Definiert den Wert der Variable für die Punkteanzahl.
        public int GamePoints;
        //Einstellung der Ballposition nach Aktivierung des Timers
        public int Xdirection = 5;
        public int Ydirection = 2;
        //Stellt die Ballposition auf 25 Pixel für die Ballkontrolle ein.
        public int IncreaseDirection = 25;
        //Beheben Sie den Bug, dass den Ball sich am Schläger festsetzt.
        public bool PaddleFix = true;
        //Legt einen Zufallswert für die Position der Ball fest.
        public static Random r = new Random();
        int Rdirection = r.Next(7, 15);

        public Form1()
        {
            InitializeComponent();
        }
        
        private void GameWindow_Load(object sender, EventArgs e)
        {
            //Definiert die Position des Schlägers auf dem Spielfeld. RightPaddle.Width*2 bringt der Schläger etwas nach vorne.
            RightPaddle.Location = new Point(GameField.Width - RightPaddle.Width * 2, GameField.Height / 2);
            //Legt die Position der Bildlaufleiste auf dem Formular fest.
            ScrollRightPaddle.Location = new Point(GameField.Location.X + GameField.Width, GameField.Location.Y);
            //Legt den Mindestwert der Bildlaufleiste fest.
            ScrollRightPaddle.Minimum = 0;
            //Legt den maximalen Wert der Bildlaufleiste fest, der sich aus der Grösse des Spielfeldes abzüglich der Höhe des Schlägers ergibt. LargeChance kompensiert den Unterschied zwischen der Bildlaufleiste und dem Schläger.
            ScrollRightPaddle.Maximum = GameField.Height - RightPaddle.Height + ScrollRightPaddle.LargeChange;
            //Definiert, dass die Höhe der Bildlaufleiste gleich der Höhe des Spielfeldes ist.
            ScrollRightPaddle.Height = GameField.Height;
            //Definiert, dass der aktuelle Wert der Bildlaufleiste gleich der vertikalen Position des Schlägers ist.
            ScrollRightPaddle.Value = RightPaddle.Location.Y;
        }

        private void ScrollRightPaddle_Scroll(object sender, ScrollEventArgs e)
        {
            //Wir haben die neue Position der Bildlaufleiste definiert: X ist die Position des RightPaddle-Bildes und Y ist der Positionswert des ScrollRightPaddle-Schlägers.
            RightPaddle.Location = new Point(RightPaddle.Location.X, ScrollRightPaddle.Value);
        }

        private void StartGame_Click(object sender, EventArgs e)
        {
            //Aktiviert und startet den Timer.
            GameTimer.Enabled = true;
            GameTimer.Start();
        }

        private void ResetGame_Click(object sender, EventArgs e)
        {
            //Rudimentäre Implementierung der Neustart-Funktion
            GameTimer.Enabled = Enabled;
            Ball.Location = new Point(100, 100);
        }

        public void GameTimer_Tick(object sender, EventArgs e)
        {
            //Wir zeigen die Punktzahl, indem wir die ganze Zahl in eine String konvertieren.
            ShowPoints.Text = GamePoints.ToString();
            //Stellt die Ballposition ein, wenn der Timer abläuft.
            Ball.Location = new Point(Ball.Location.X + Xdirection, Ball.Location.Y + Ydirection);
            //Der Ball spiegelt den negativen Wert der X-Richtung wider, wenn er den richtigen Schläger trifft und der Spieler 10 Punkte erhält. Der "right" Bool dient dazu, den Bug zu entfernen, wenn der Ball den Schläger trifft.
            if (Ball.Location.X + Ball.Width >= GameField.Width - RightPaddle.Width * 2 && PaddleFix &&
                Ball.Location.Y + Ball.Height >= RightPaddle.Location.Y &&
                Ball.Location.Y <= RightPaddle.Location.Y + RightPaddle.Height)
            {
                Xdirection = -Xdirection;
                PaddleFix = false;
                GamePoints += 10;
            }
            //Wenn der Ball das Spielfeld verlässt, der Timer stoppt und das Game Over-Formular erscheint.
            else if (Ball.Location.X > GameField.Width)
            {
                GameTimer.Enabled = false;
                GameTimer.Stop();
                GameOverWindow frm = new GameOverWindow();
                frm.Show();
            }

            //Der Ball reflektiert den negativen Wert der Y-Richtung, wenn er auf die untere Wand trifft.
            else if (Ball.Location.Y >= GameField.Height - Ball.Height)
            {
                Ydirection = -Ydirection;
            }
            //Der Ball reflektiert den negativen Wert in X-Richtung, wenn er auf die linke Wand trifft. Wir haben die Variable "right" reaktiviert, um sicherzustellen, dass der Ball nicht durch den Schläger herauskommt.
            else if (Ball.Location.X <= 0)
            {
                PaddleFix = true;
                Xdirection = -Xdirection;
            }
            //Der Ball reflektiert den negativen Wert in Y-Richtung, wenn er auf die obere Wand trifft. Wir haben die Variable "right" reaktiviert, um sicherzustellen, dass der Ball nicht durch den Schläger herauskommt.
            else if (Ball.Location.Y <= 0)
            {
                Ydirection = -Ydirection;
            }
        }
        
        //Die Kugel reflektiert den negativen Wert der Y-Richtung, wenn sie die obere Wand erreicht.
        private void MoveBall(object sender, EventArgs e)
        {
            switch (((Button) sender).Name)
            {
                case "MoveUp":
                    Ball.Location = new Point(Ball.Location.X, Ball.Location.Y - IncreaseDirection);
                    PaddleFix = true;
                    break;
                case "MoveDown":
                    Ball.Location = new Point(Ball.Location.X, Ball.Location.Y + IncreaseDirection);
                    PaddleFix = true;
                    break;
                case "MoveLeft":
                    Ball.Location = new Point(Ball.Location.X - IncreaseDirection, Ball.Location.Y);
                    PaddleFix = true;
                    break;
                case "MoveRight":
                    Ball.Location = new Point(Ball.Location.X + IncreaseDirection, Ball.Location.Y);
                    PaddleFix = true;
                    break;
            }
        }
        
        protected override bool ProcessDialogKey(Keys keyData)
        {
            //Wenn Sie den Pfeil nach oben drücken, geht die Kugel 25 Pixel nach oben. Wir haben die Variable reaktiviert, um sicherzustellen, dass der Ball nicht aus dem Schläger kommt.
            if (keyData == Keys.Up)
            {
                MoveBall(this.MoveUp, null);
                return true;
            }
            //Wenn Sie den Pfeil nach unten drücken, geht die Kugel 25 Pixel nach oben. Wir haben die Variable reaktiviert, um sicherzustellen, dass der Ball nicht aus dem Schläger kommt.
            else if (keyData == Keys.Down)
            {
                MoveBall(this.MoveDown, null);
                return true;
            }
            //Wenn Sie den Pfeil nach links drücken, geht die Kugel 25 Pixel nach oben. Wir haben die Variable reaktiviert, um sicherzustellen, dass der Ball nicht aus dem Schläger kommt.
            else if (keyData == Keys.Left)
            {
                MoveBall(this.MoveLeft, null);
                return true;
            }
            //Wenn Sie den Pfeil nach rechts drücken, geht die Kugel 25 Pixel nach oben. Wir haben die Variable reaktiviert, um sicherzustellen, dass der Ball nicht aus dem Schläger kommt.
            else if (keyData == Keys.Right)
            {
                MoveBall(this.MoveRight, null);
                return true;
            }
            //Wenn Sie die P-Taste drücken, wird das Spiel angehalten.
            else if (keyData == Keys.P)
            {
                GameTimer.Stop();
                return true;
            }
            //Wenn Sie die S-Taste drücken, wird das Spiel nach einer Pause fortgesetzt.
            else if (keyData == Keys.S)
            {
                GameTimer.Start();
                return true;
            }
            //Wenn die Taste H gedrückt wird, kehrt sich die horizontale Richtung der Kugel um. Wir haben die Variable reaktiviert, um sicherzustellen, dass der Ball nicht durch den Schläger herauskommt.
            else if (keyData == Keys.H)
            {
                Xdirection = -Xdirection;
                PaddleFix = true;
                return true;
            }
            //Wenn die Taste V gedrückt wird, kehrt sich die vertikale Richtung der Kugel um. Wir haben die Variable reaktiviert, um sicherzustellen, dass der Ball nicht durch den Schläger herauskommt.
            else if (keyData == Keys.V)
            {
                Ydirection = -Ydirection;
                PaddleFix = true;
                return true;
            }
            else return base.ProcessDialogKey(keyData);
        }

        //Aktiviert der Ballsteuerung.
        private void BallControl_CheckedChanged(object sender, EventArgs e)
        {
            ScrollRightPaddle.Enabled = true;
            this.MoveUp.Enabled = false;
            this.MoveDown.Enabled = false;
            this.MoveLeft.Enabled = false;
            this.MoveRight.Enabled = false;
        }
        //Aktiviert der Schlägersteuerung.
        private void PaddleControl_CheckedChanged(object sender, EventArgs e)
        {
            ScrollRightPaddle.Enabled = false;
            this.MoveUp.Enabled = true;
            this.MoveDown.Enabled = true;
            this.MoveLeft.Enabled = true;
            this.MoveRight.Enabled = true;
        }
    }
}