using System.ComponentModel;

namespace pingpong
{
    partial class GameOverWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ScorePoints = new System.Windows.Forms.Label();
            this.Scoreboard = new System.Windows.Forms.Label();
            this.SubmitScore = new System.Windows.Forms.Button();
            this.Back2Game = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Punkte:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(108, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "Name:";
            // 
            // ScorePoints
            // 
            this.ScorePoints.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ScorePoints.Location = new System.Drawing.Point(12, 36);
            this.ScorePoints.Name = "ScorePoints";
            this.ScorePoints.Size = new System.Drawing.Size(72, 23);
            this.ScorePoints.TabIndex = 2;
            // 
            // Scoreboard
            // 
            this.Scoreboard.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.Scoreboard.Location = new System.Drawing.Point(12, 77);
            this.Scoreboard.Name = "Scoreboard";
            this.Scoreboard.Size = new System.Drawing.Size(325, 332);
            this.Scoreboard.TabIndex = 3;
            // 
            // SubmitScore
            // 
            this.SubmitScore.Location = new System.Drawing.Point(260, 36);
            this.SubmitScore.Name = "SubmitScore";
            this.SubmitScore.Size = new System.Drawing.Size(79, 23);
            this.SubmitScore.TabIndex = 4;
            this.SubmitScore.Text = "Eintragen";
            this.SubmitScore.UseVisualStyleBackColor = true;
            // 
            // Back2Game
            // 
            this.Back2Game.Location = new System.Drawing.Point(260, 421);
            this.Back2Game.Name = "Back2Game";
            this.Back2Game.Size = new System.Drawing.Size(79, 38);
            this.Back2Game.TabIndex = 5;
            this.Back2Game.Text = "Zurück";
            this.Back2Game.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(108, 36);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(132, 23);
            this.textBox1.TabIndex = 6;
            // 
            // GameOverWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 467);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Back2Game);
            this.Controls.Add(this.SubmitScore);
            this.Controls.Add(this.Scoreboard);
            this.Controls.Add(this.ScorePoints);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "GameOverWindow";
            this.Text = "Game Over!";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ScorePoints;
        private System.Windows.Forms.Label Scoreboard;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Back2Game;
        private System.Windows.Forms.Button SubmitScore;
    }
}